# frozen_string_literal: true

class FetchRemoteStatusService < BaseService
  def call(url, prefetched_body = nil, on_behalf_of = nil)
    if prefetched_body.nil?
      resource_url, resource_options = FetchResourceService.new.call(url, on_behalf_of: on_behalf_of)
    else
      resource_url     = url
      resource_options = { prefetched_body: prefetched_body }
    end

    resource_options ||= {}
    resource_options[:on_behalf_of] = on_behalf_of

    ActivityPub::FetchRemoteStatusService.new.call(resource_url, **resource_options) unless resource_url.nil?
  end
end
