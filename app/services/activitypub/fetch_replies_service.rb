# frozen_string_literal: true

class ActivityPub::FetchRepliesService < BaseService
  def call(parent_status, collection_or_uri, allow_synchronous_requests = true)
    @parent_status = parent_status
    @collection_or_uri = collection_or_uri
    @allow_synchronous_requests = allow_synchronous_requests

    items = fetch_collection_items
    return if items.blank?

    FetchReplyWorker.push_bulk(items)
    items
  end

  private

  def fetch_collection_items
    ActivityPub::FetchCollectionItemsService.new.call(
      @collection_or_uri, @parent_status&.account,
      page_limit: 1,
      item_limit: 20,
      allow_synchronous_requests: @allow_synchronous_requests
    )
  end
end
