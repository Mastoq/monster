# frozen_string_literal: true

class FetchReplyWorker
  include Sidekiq::Worker
  include ExponentialBackoff

  sidekiq_options queue: 'pull', retry: 3

  def perform(child_url)
    if child_url.is_a?(String)
      FetchRemoteStatusService.new.call(child_url, nil)
    elsif child_url.is_a?(Enumerable)
      child_url.each { |url| FetchRemoteStatusService.new.call(url, nil) }
    end
  end
end
