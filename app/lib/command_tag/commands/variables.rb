# frozen_string_literal: true

module CommandTag::Commands::Variables
  def handle_set_at_start(args)
    return if args.blank?

    args[0] = normalize(args[0])

    case args.count
    when 1
      @vars.delete(args[0])
    else
      @vars[args[0]] = args[1..-1]
    end
  end

  def do_unset_at_start(args)
    args.each do |arg|
      @vars.delete(normalize(arg))
    end
  end
end
