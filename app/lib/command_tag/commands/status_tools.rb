# frozen_string_literal: true
module CommandTag::Commands::StatusTools
  def handle_title_before_save(args)
    return unless author_of_status?

    @status.title = args[0]
  end

  def handle_visibility_before_save(args)
    return unless author_of_status? && args[0].present?

    args[0] = read_visibility_from(args[0])
    return if args[0].blank?

    if args[1].blank?
      @status.visibility = args[0].to_sym
    elsif args[0] == @status.visibility.to_s
      domains = args[1..-1].map { |domain| normalize_domain(domain) }.uniq.compact
      @status.domain_permissions.where(domain: domains).destroy_all if domains.present?
    else
      args[1..-1].flat_map(&:split).uniq.each do |domain|
        domain = normalize_domain(domain)
        @status.domain_permissions.create_or_update(domain: domain, visibility: args[0]) if domain.present?
      end
    end
  end

  alias handle_v_before_save                      handle_visibility_before_save
  alias handle_privacy_before_save                handle_visibility_before_save
end
