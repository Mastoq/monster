# frozen_string_literal: true

module CommandTag::Commands::TextTools
  def handle_code_at_start(args)
    name = args.count > 1 ? args[0] : 'code'

    value = args.last.presence || ''
    @vars[name] = case @status.content_type
                  when 'text/markdown'
                    ["```\n#{value}\n```"]
                  when 'text/html'
                    ["<pre><code>#{html_encode(value).gsub("\n", '<br/>')}</code></pre>"]
                  else
                    ["----------\n#{value}\n----------"]
                  end
  end

  def handle_prepend_before_save(args)
    args.each { |arg| @text = "#{arg}\n#{text}" }
  end

  def handle_append_before_save(args)
    args.each { |arg| @text << "\n#{arg}" }
  end

  def handle_replace_before_save(args)
    @text.gsub!(args[0], args[1] || '')
  end

  alias handle_sub_before_save handle_replace_before_save

  def handle_regex_replace_before_save(args)
    flags     = normalize(args[2])
    re_opts   = (flags.include?('i') ? Regexp::IGNORECASE : 0)
    re_opts  |= (flags.include?('x') ? Regexp::EXTENDED : 0)
    re_opts  |= (flags.include?('m') ? Regexp::MULTILINE : 0)

    @text.gsub!(Regexp.new(args[0], re_opts), args[1] || '')
  end

  alias handle_resub_before_save handle_replace_before_save
  alias handle_regex_sub_before_save handle_replace_before_save
end
