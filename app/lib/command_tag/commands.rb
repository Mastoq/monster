# frozen_string_literal: true
module CommandTag::Commands
  def self.included(base)
    CommandTag::Commands.constants.map(&CommandTag::Commands.method(:const_get)).grep(Module) do |mod|
      base.include(mod)
    end
  end
end
