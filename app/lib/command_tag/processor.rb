# frozen_string_literal: true

#                  .~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~.                  #
###################              Cthulhu Code!              ###################
#                  `~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~`                  #
# - Interprets and executes user input.  THIS CAN BE VERY DANGEROUS!          #
# - Has a high complexity level and needs tests.                              #
# - May destroy objects passed to it.                                         #
# - Incurs a high performance penalty.                                        #
#                                                                             #
###############################################################################

class CommandTag::Processor
  include Redisable
  include ImgProxyHelper
  include CommandTag::Commands

  MENTIONS_OR_HASHTAGS_RE = /(?:(?:#{Account::MENTION_RE}|#{Tag::HASHTAG_RE})\s*)+/.freeze
  PARSEABLE_RE = /^\s*(?:#{MENTIONS_OR_HASHTAGS_RE})?#!|%%.+?%%/.freeze
  STATEMENT_RE = /^\s*#!\s*[^\n]+ (?:start|begin|do)$.*?\n\s*#!\s*(?:end|stop|done)\s*$|^\s*#!\s*.*?\s*$/im.freeze
  TEMPLATE_RE = /%%\s*(.*?)\s*%%/.freeze
  ESCAPE_MAP = {
    '\n' => "\n",
    '\r' => "\r",
    '\t' => "\t",
    '\\\\' => '\\',
  }.freeze

  def initialize(account, status)
    @account      = account
    @status       = status
    @parent       = status.thread
    @conversation = status.conversation
    @run_once     = Set[]
    @vars         = { 'statement_uuid' => [nil] }
    @text         = prepare_input(status.text)
    @statements   = {}

    return unless @account.present? && @account.local? && @status.present?
  end

  def process!
    unless @text.match?(PARSEABLE_RE)
      process_inline_images!
      @status.save!
      return
    end

    reset_status_caches

    initialize_handlers!

    parse_statements

    execute_statements(:at_start)
    execute_statements(:with_return, true)
    @text = parse_templates(@text).rstrip
    execute_statements(:before_save)

    if @text.blank? || @text.gsub(MENTIONS_OR_HASHTAGS_RE, '').strip.blank?
      execute_statements(:when_blank)

      unless (@status.published? && !@status.edited.zero?) || @text.present?
        execute_statements(:before_destroy)
        @status.destroy
        execute_statements(:after_destroy)
      end
    elsif @status.destroyed?
      execute_statements(:after_destroy)
    else
      @status.text = @text
      process_inline_images!
      if @status.save
        execute_statements(:after_save)
      else
        execute_statements(:after_save_fail)
      end
    end

    execute_statements(:at_end)
    reset_status_caches
  end

  private

  def initialize_handlers!
    self.class.instance_methods.grep(/\Ainitialize_\w+!\z/).each do |name|
      public_send(name)
    end
  end

  def prepare_input(text)
    text.gsub(/\r\n|\n\r|\r/, "\n").gsub(/^\s*(#{MENTIONS_OR_HASHTAGS_RE})#!/, "\\1\n#!")
  end

  def parse_templates(text)
    text.gsub(TEMPLATE_RE) do
      return if Regexp.last_match(1).blank?

      template = Regexp.last_match(1).scan(/'([^']*)'|"([^"]*)"|(\S+)/).flatten.compact
      return if template[0].blank?

      name      = normalize(template[0])
      separator = "\n"

      if template.count > 2
        if %w(by with using sep separator delim delimiter).include?(template[-2].downcase)
          separator = unescape_literals(template[-1])
          template = template[0..-3]
        elsif !template[-1].match?(/\A[-+]?[0-9]+\z/)
          separator = unescape_literals(template[-1])
          template.pop
        end
      end

      index_start = to_integer(template[1])
      index_end   = to_integer(template[2])

      if ['all', '[]'].include?(template[1])
        var(name).join(separator)
      elsif index_end.zero?
        var(name)[index_start].presence || ''
      else
        var(name)[index_start..index_end].presence || ''
      end
    end
  end

  def parse_statements
    @text.gsub!(STATEMENT_RE) do
      statement = Regexp.last_match(0).strip[2..-1]
      next if statement.blank?

      statement_array = statement.scan(/'([^']*)'|"([^"]*)"|(\S+)|\s+(?:start|begin|do)\s*$\n+(.*)/im).flatten.compact
      statement_array[0] = statement_array[0].strip.tr(':.\- ', '_').gsub(/__+/, '_').downcase
      next unless statement_array[0].match?(/\A[\w_]+\z/)

      statement_array[-1].rstrip! if statement_array.count > 1
      add_statement_handlers_for(statement_array)
    end
  end

  def potential_handlers_for(name)
    ['_once', ''].each_with_index do |count_affix, index|
      %w(at_start with_return when_blank at_end).each do |when_affix|
        yield ["#{count_affix}_#{when_affix}", "handle_#{name}#{count_affix}_#{when_affix}", index.zero?]
      end

      %w(destroy save postprocess save_fail).each do |event_affix|
        %w(before after).each do |when_affix|
          yield ["#{count_affix}_#{when_affix}_#{event_affix}", "handle_#{name}#{count_affix}_#{when_affix}_#{event_affix}", index.zero?]
        end
      end
    end
  end

  def add_statement_handlers_for(statement_array)
    statement_uuid = SecureRandom.uuid

    potential_handlers_for(statement_array[0]) do |when_affix, handler, once|
      if !(once && @run_once.include?(handler)) && respond_to?(handler)
        @statements[when_affix] ||= []
        @statements[when_affix] << [handler, statement_array[1..-1], statement_uuid]
        @run_once << handler if once
      end
    end

    # Template for statement return value.
    "%% statement:#{statement_uuid} %%"
  end

  def execute_statements(event, with_return = false)
    ["_#{event}", "_once_#{event}"].each do |when_affix|
      next if @statements[when_affix].blank?

      @statements[when_affix].each do |handler, arguments, uuid|
        @vars['statement_uuid'][0] = uuid
        if with_return
          @vars["statement:#{uuid}"] = [public_send(handler, arguments)]
        else
          public_send(handler, arguments)
        end
      end
    end
  end

  def reset_status_caches
    [@status, @parent].each do |status|
      next unless @account.id == status&.account_id

      Rails.cache.delete_matched("statuses/#{status.id}-*")
      Rails.cache.delete("statuses/#{status.id}")
      Rails.cache.delete(status)
      redis.zremrangebyscore("spam_check:#{status.account.id}", status.id, status.id)
    end
  end

  def author_of_status?
    @account.id == @status.account_id
  end

  def author_of_parent?
    @account.id == @parent&.account_id
  end

  def destroy_status!
    @status.destroy unless @status.destroyed?
  end

  def normalize(text)
    text.to_s.strip.downcase
  end

  def to_integer(text)
    text&.strip.to_i
  end

  def unescape_literals(text)
    ESCAPE_MAP.each { |escaped, unescaped| text.gsub!(escaped, unescaped) }
    text
  end

  def html_encode(text)
    (@html_entities ||= HTMLEntities.new).encode(text)
  end

  def var(name)
    @vars[name].presence || []
  end

  def read_visibility_from(arg)
    return if arg.strip.blank?

    arg = case arg.strip
          when 'p', 'pu', 'all', 'world'
            'public'
          when 'u', 'ul'
            'unlisted'
          when 'f', 'follower', 'followers', 'packmates', 'follower-only', 'followers-only', 'packmates-only'
            'private'
          when 'd', 'dm', 'pm', 'directmessage'
            'direct'
          when 'default', 'reset'
            @account.user.setting_default_privacy
          else
            arg.strip
          end

    %w(public unlisted private limited direct).include?(arg) ? arg : nil
  end

  def read_falsy_from(arg)
    %w(false no off disable).include?(arg)
  end

  def read_truthy_from(arg)
    %w(true yes on enable).include?(arg)
  end

  def read_boolean_from(arg)
    arg.present? && (read_truthy_from(arg) || !read_falsy_from(arg))
  end

  def normalize_domain(domain)
    return if domain&.strip.blank? || !domain.include?('.')

    domain.split('.').map(&:strip).reject(&:blank?).join('.').downcase
  end

  def federating_with_domain?(domain)
    return false if domain.blank?

    DomainAllow.where(domain: domain).exists? || Account.where(domain: domain, suspended_at: nil).exists?
  end
end
